ActiveAdmin.register SentReport do
  permit_params :sent_to, :company_name, :document, :job_title, :postal_code

  actions :all, :except => [:destroy]

  index do
    column :id
    column :company_name
    column :postal_code
    column :job_title
    column :sent_to
    column('Report') { |sr|
      link_to('Download PDF', sr.document.url, :target => "_blank")
    }
    column('Created At') { |sr|
      sr.created_at.localtime.strftime("%B %d, %Y %I:%M %P")
    }

    actions
  end

  show do |sr|
    attributes_table do
      row :company_name
      row :postal_code
      row :job_title
      row :sent_to
      row :document do
        link_to('Download PDF', sr.document.url, :target => "_blank")
      end

      # row :instruction do
      #   item.instruction? ? link_to(item.instruction_file_name, item.instruction.url) : content_tag(:span, "No instruction file yet")
      #      end
      #    row :photo do
      #      item.photo? ? image_tag(item.photo.url, height: '100') : content_tag(:span, "No photo yet")
      #    end
      # end
    end
    active_admin_comments
  end

  form do |f|
    f.inputs do
      f.input :company_name
      f.input :postal_code
      f.input :job_title
      f.input :sent_to
      f.input :document
    end
    actions
  end



end
