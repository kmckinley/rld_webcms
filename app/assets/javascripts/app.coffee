rld = angular.module('rld',[
  'controllers',
  'directives',
  'services',
  'templates',
  'ngRoute',
  'ngResource',
  'ngCookies',
  'angular-flash.service',
  'angular-flash.flash-alert-directive',
  'angularUtils.directives.dirPagination',
  'pascalprecht.translate',
  'angular-google-analytics'
])


rld.config([ '$routeProvider', 'flashProvider', '$translateProvider', 'AnalyticsProvider',
  ($routeProvider,flashProvider,$translateProvider,AnalyticsProvider)->

#    AnalyticsProvider.setAccount(
#      {
#        tracker: 'UA-12345-12',
#        name: "tracker1",
#        fields: {
#          cookieDomain: 'foo.example.com',
#          cookieName: 'myNewName',
#          cookieExpires: 20000
#        },
#        crossDomainLinker: true,
#        crossLinkDomains: ['domain-1.com', 'domain-2.com'],
#        displayFeatures: true,
#        enhancedLinkAttribution: true,
#        select: (args)-> true,
#        set: {
#          forceSSL: true
#        },
#        trackEvent: true,
#        trackEcommerce: false
#      }
#    )

    flashProvider.errorClassnames.push("alert-danger")
    flashProvider.warnClassnames.push("alert-warning")
    flashProvider.infoClassnames.push("alert-info")
    flashProvider.successClassnames.push("alert-success")

    $translateProvider.useStaticFilesLoader({
      prefix: '/languages/locale-',
      suffix: '.json'
    })
    $translateProvider.preferredLanguage('en_US')
    $translateProvider.useSanitizeValueStrategy('sanitizeParameters');

    $routeProvider
        .when('/',
          templateUrl: "home/index.html"
          controller: 'HomeController'
        )
])

angular.module('controllers',[])
angular.module('directives', [])
angular.module('services', [])