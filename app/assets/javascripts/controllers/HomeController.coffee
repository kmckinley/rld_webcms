controllers = angular.module('controllers')

controllers.controller('HomeController', [ '$scope', '$cookies', 'ApiRestServices', 'Analytics',
  ($scope,$cookies,ApiRestServices,Analytics)->

    # ------------------------------------------------
    # Pressure Booster
    if $cookies.getObject('input_pb')
      $scope.input_pb = $cookies.getObject('input_pb')
    else
      $scope.input_pb = {
        a: 40,b: 45,c: 108,d: 200,e: 5,f: 0,g: 40,h: 65,i: 0,j: 20,k: 5,l: 0,m: 10,n: 9,o: 0
      }
    $scope.solution_pb =  {
      p: 0,q: 0,r: 0,s: 0,t: 0,u: 0,v1: 0,v2: 0
    }

    $scope.input_pb_changed = (input)->
      $cookies.putObject('input_pb', $scope.input_pb)
      value = (parseFloat($scope.input_pb.b)+(parseFloat($scope.input_pb.c)/2.31)+(((parseFloat($scope.input_pb.d)+parseFloat($scope.input_pb.c))/100)*parseFloat($scope.input_pb.e))+parseFloat($scope.input_pb.f))
      $scope.solution_pb.p = value
      value = parseFloat($scope.input_pb.g)-(parseFloat($scope.input_pb.i)/2.31)-(((parseFloat($scope.input_pb.i)+parseFloat($scope.input_pb.j))/100)*parseFloat($scope.input_pb.e))-parseFloat($scope.input_pb.k)-parseFloat($scope.input_pb.m)-parseFloat($scope.input_pb.o)
      $scope.solution_pb.q = value
      value = parseFloat($scope.solution_pb.p) - parseFloat($scope.solution_pb.q)
      $scope.solution_pb.r = value
      value = (parseFloat($scope.input_pb.a) + (parseFloat($scope.input_pb.c)/2.31))
      $scope.solution_pb.s = value
      value = ((parseFloat($scope.input_pb.h)-(parseFloat($scope.input_pb.i)/2.31)-parseFloat($scope.input_pb.l))-parseFloat($scope.input_pb.n))
      $scope.solution_pb.t = value
      value = parseFloat($scope.solution_pb.s) - parseFloat($scope.solution_pb.t)
      $scope.solution_pb.u = value
      $scope.solution_pb.v1 = Math.abs(parseFloat($scope.solution_pb.s))
      $scope.solution_pb.v2 = (parseFloat($scope.solution_pb.v1)-(parseFloat($scope.input_pb.c)/2.31))

      # Call tank selection changed method so items dependant on pressure booster get updated
      $scope.input_ts_changed()






    # ------------------------------------------------
    # Tank Selection
    $scope.tanks = []

    $scope.tank_locations = {
      list: [
        {id: 1, name: 'Discharge'},
        {id: 2, name: 'Top'}
      ],
      selected: {}
    }
    $scope.tank_locations.selected = $scope.tank_locations.list[0]

    $scope.building_types = {
      list: [],
      selected: {}
    }

    $scope.input_ts = {
      a: 15,b: 200
    }
    $scope.solution_ts =  {
      e: 0,f: 0,g: 0,h: 0,i: 0,j: 0,j_desc: 'None',j_link: null
    }

    ApiRestServices.tanks(
      (results)->
        $scope.tanks = results
    )

    ApiRestServices.building_types(
      (results)->
        $scope.building_types.selected = results[0]
        $scope.building_types.list = results
        $scope.input_ts_changed()
    )

    $scope.input_ts_changed = (input)->
      input_a = parseFloat($scope.input_ts.a)
      input_b = parseFloat($scope.input_ts.b)
      input_c = $scope.building_types.selected.multiplier

      value = Math.abs((input_b/200)*input_c*input_a/30)
      $scope.solution_ts.e = value

      if $scope.tank_locations.selected.id == 1
        $scope.solution_ts.f = $scope.solution_pb.v1
        value = Math.abs(parseFloat($scope.solution_pb.p))
        $scope.solution_ts.g = value
      else
        $scope.solution_ts.f = $scope.solution_pb.v2
        value = Math.abs(parseFloat($scope.solution_pb.p-(parseFloat($scope.input_pb.c)/2.31)))
        $scope.solution_ts.g = value

      value = 1-((parseFloat($scope.solution_ts.f)+14.7)/(parseFloat($scope.solution_ts.g)+14.7))
      $scope.solution_ts.h = value

      value = Math.abs(parseFloat($scope.solution_ts.e)/parseFloat($scope.solution_ts.h))
      $scope.solution_ts.i = value

      found = false
      for tank in $scope.tanks
        if tank.volume >= parseFloat($scope.solution_ts.i) && !found
          found = true
          $scope.solution_ts.j = tank.volume
          $scope.solution_ts.j_desc = tank.description
          $scope.solution_ts.j_link = tank.link

      if !found
        $scope.solution_ts.j = 0
        $scope.solution_ts.j_desc = 'none'
        $scope.solution_ts.j_link = null




    $scope.input_pb_changed()
    $scope.input_ts_changed()


    $scope.toggle_modal = ->
      $scope.showModal = !$scope.showModal


    $scope.email_report_model = ()->
      $scope.modalState = "email_report"
      $scope.toggle_modal()

    $scope.email_results = (company_name, postal_code, job_title, email)->
      if company_name && email && postal_code
        ApiRestServices.email_results({company_name: company_name, postal_code: postal_code, job_title: job_title, email: email, input_pb: $scope.input_pb, solution_pb: $scope.solution_pb, input_ts: $scope.input_ts, building_type: $scope.building_types.selected['name'], pump_location: $scope.tank_locations.selected['name'], solution_ts: $scope.solution_ts},
          (email_results)->
            $scope.toggle_modal()

          (email_results_errors)->
            $scope.toggle_modal()
        )


    $scope.reset_model = ()->
      $scope.modalState = "reset_confirm"
      $scope.toggle_modal()

    $scope.reset = ->
      $scope.input_pb = {
        a: 40,b: 45,c: 108,d: 200,e: 5,f: 0,g: 40,h: 65,i: 0,j: 20,k: 5,l: 0,m: 10,n: 9,o: 0
      }
      $scope.input_ts = {
        a: 15,b: 200
      }
      $scope.tank_locations.selected = $scope.tank_locations.list[0]
      $scope.building_types.selected = $scope.building_types.list[0]
      $scope.input_pb_changed()
      $scope.input_ts_changed()
      $cookies.putObject('input_pb', $scope.input_pb)
      $scope.toggle_modal()
])
