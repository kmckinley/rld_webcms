directives = angular.module('rld')

directives.directive('kmNumericShowError', ->
  restrict: 'EA'
  link: (scope, element, attrs) ->
    inputEl   = element[0].querySelector("input")
    inputNgEl = angular.element(inputEl)
    inputNgEl.bind('blur',
      ()->
        value = inputNgEl.val()
        isNumeric = !isNaN(value+0) && value != null && value != ''
        element.toggleClass('has-error', !isNumeric);
    )
)