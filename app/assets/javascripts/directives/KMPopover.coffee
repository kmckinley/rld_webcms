directives = angular.module('rld')

directives.directive('kmPopover',['$translate'
  ($translate)->
    restrict: 'EA'
    link: (scope, element, attrs) ->
      $translate(attrs.popoverTranslate).then((translation) ->
        content = ''
        if attrs.startHtml
          content += attrs.startHtml

        content += translation

        if attrs.endHtml
          content += attrs.endHtml

        element.popover( {
          trigger: 'click',
          html: true,
          content: content,
          placement: attrs.popoverPlacement
        })
      )
])