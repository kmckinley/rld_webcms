directives = angular.module('rld')

directives.directive('modal', ->
  restrict: 'EA'
  templateUrl: "directives/modal.html"
  transclude: true
  replace: true
  link: (scope, element, attrs) ->
    scope.title = attrs.title
    scope.$watch attrs.visible, (value)->
      if value == true
        element.modal 'show'
      else
        element.modal 'hide'

    element.on 'shown.bs.modal', ->
      scope.$apply ->
        scope.$parent[attrs.visible] = true

    element.on 'hidden.bs.modal', ->
      scope.$apply ->
        scope.$parent[attrs.visible] = false
)
