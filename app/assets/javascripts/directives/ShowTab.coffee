directives = angular.module('rld')

directives.directive('showTab', ->
  restrict: 'EA'
  link: (scope, element, attrs) ->
    element.click (e) ->
      e.preventDefault()
      element.tab 'show'
)