api_rest_service = angular.module('services');

api_rest_service.factory('ApiRestServices', ['$resource',
  ($resource) ->
    $resource('/', { format: 'json' }, {
      tanks: {
        url: '/tanks/index',
        isArray: true
      },
      building_types: {
        url: '/building_types/index',
        isArray: true
      },
      email_results: {
        url: '/home/email_results',
        isArray: false
      }
    })
])