class BuildingTypesController < ApplicationController

  def index
    @building_types = BuildingType.all.order(:name)
  end
end
