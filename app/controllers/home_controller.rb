class HomeController < ApplicationController

  def index
  end

  def email_results
    input_pb = params[:input_pb]
    solution_pb = params[:solution_pb]
    input_ts = params[:input_ts]
    building_type = params[:building_type]
    pump_location = params[:pump_location]
    solution_ts = params[:solution_ts]
    company_name = params[:company_name]
    postal_code = params[:postal_code]
    job_title = params[:job_title]
    email = params[:email]
    lang = params[:lang]
    pdf_builder = PdfReport.new(email, company_name, postal_code, job_title, lang, input_pb, solution_pb, input_ts, building_type, pump_location, solution_ts)
    pdf_builder.delay.build
    render :json => { :pending => 'Request pending.' }, :status => 200
  end

end