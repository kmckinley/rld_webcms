require 'net/http'
require 'uri'

class PdfMailer < ActionMailer::Base

  default :from => 'support@xylemsales.com'

  def email_report(to_emails, file_name, file)
    unless file.nil?
      attachments.inline[file_name] = file
    end
    mail(subject: "Pressure Booster Report",
         to: to_emails, cc: [], bcc: []
    )

    headers['X-MC-Template'] = 'pressure_booster_report'

    # Tags help classify your messages
    headers['X-MC-Tags'] = 'RLD PDF Report'

    # Enable open or click-tracking for the message.
    # Only can track to at a time. Possibilies: opens, clicks, clicks_htmlonly, clicks_textonly
    headers['X-MC-Track'] = 'opens, clicks'

    # Automatically generate a plain-text version of the email from the HTML content.
    headers['X-MC-Autotext'] = 'true'

    # Whether to show recipients of the email other recipients, such as those in the "cc" field. "true" or "false"
    headers['X-MC-PreserveRecipients'] = 'false'

  end

  def email_reportXXX(email, file, file_name)

    # Name of template in your Mandrill template area
    headers['X-MC-Template'] = 'pressure_booster_report'

    # Tags help classify your messages
    headers['X-MC-Tags'] = 'RLD PDF Report'

    # Enable open or click-tracking for the message.
    # Only can track to at a time. Possibilies: opens, clicks, clicks_htmlonly, clicks_textonly
    headers['X-MC-Track'] = 'opens, clicks'

    # Automatically generate a plain-text version of the email from the HTML content.
    headers['X-MC-Autotext'] = 'true'

    # Whether to show recipients of the email other recipients, such as those in the "cc" field. "true" or "false"
    headers['X-MC-PreserveRecipients'] = 'false'

    message = prepare_message   :subject => "Pressure Booster Report",
                                :to      => email,
                                :content_type => "multipart/mixed"

    message.alternative_content_types_with_attachment(
        :text => 'text',
        :html => 'html'
    ) do |i|
      # Parse the S3 URL into its constituent parts
      # uri = URI.parse file_url
      # # Use Ruby's built-in Net::HTTP to read the attachment into memory
      # response = Net::HTTP.start(uri.host, uri.port) { |http| http.get uri.path }
      # Attach it to your outgoing ActionMailer email
      i.inline["#{file_name}.pdf"] = file #response.body
    end
    message
  end
end
