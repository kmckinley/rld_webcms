class SentReport < ActiveRecord::Base
  has_attached_file :document,
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :s3_region => ENV['S3_REGION'] || 'us-east-1',
                    :path => "#{ENV['S3_BASE_FOLDER']}/sent_reports/:id.pdf",
                    :default_url => ""
  validates_attachment :document, :content_type => {:content_type => %w(application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document)}

end
