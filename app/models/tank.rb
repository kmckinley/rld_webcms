class Tank < ActiveRecord::Base

  before_save :ensure_valid_link

  def ensure_valid_link
    if self.link && self.link.size > 0 && !self.link.include?('http')
      self.link = 'http://' + self.link
    end
  end

end
