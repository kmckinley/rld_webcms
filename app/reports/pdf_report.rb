class PdfReport
  @company_name = nil
  @postal_code = nil
  @job_title = nil
  @input_pb = nil
  @solution_pb = nil
  @input_ts = nil
  @building_type = nil
  @pump_location = nil
  @solution_ts = nil
  @lang = nil
  @lang_json = nil
  @email = nil

  def initialize(email, company_name, postal_code, job_title, lang, input_pb, solution_pb, input_ts, building_type, pump_location, solution_ts)
    @company_name = company_name
    @postal_code = postal_code
    @job_title = job_title
    @input_pb = JSON.parse(input_pb)
    @solution_pb = JSON.parse(solution_pb)
    @input_ts = JSON.parse(input_ts)
    @solution_ts = JSON.parse(solution_ts)
    @building_type = building_type
    @pump_location = pump_location
    @email = email
    @lang = lang
  end

  def build
    if @lang
      @lang_json = JSON.parse(File.read(Rails.root.join('public', 'languages', "locale-#{lang}.json")))
    else
      @lang_json = JSON.parse(File.read(Rails.root.join('public', 'languages', 'locale-en_US.json')))
    end

    Prawn::Document.generate("#{Rails.root}/tmp/pressure_booster_report.pdf") do |pdf|
      cover_page(pdf)
      pressure_booster_section(pdf)

      # Temporarily hidden
      # We removed tanks from the web app and shouldn't show here.  Might be brought back in the future.
      # tank_selection_section(pdf)

      attachement = pdf.render

      file = StringIO.new(attachement) #mimic a real upload file
      file.class.class_eval { attr_accessor :original_filename, :content_type } #add attr's that paperclip needs
      file.original_filename = "pressure_booster_report.pdf"
      file.content_type = "application/pdf"

      sent_report = SentReport.new
      sent_report.company_name = @company_name
      sent_report.postal_code = @postal_code
      sent_report.job_title = @job_title
      sent_report.sent_to = @email
      sent_report.document = file
      sent_report.save

      # PdfMailer.email_report(@email, attachement, 'pressure_booster_report', sent_report.document.url).deliver_now
      PdfMailer.email_report([@email], 'pressure_booster_report.pdf', attachement).deliver_now
    end
  end





  private

  def cover_page(pdf)
    # stroke_axis
    logo = Rails.root.join('app', 'assets', 'images', 'bell-gossett.png')

    pdf.fill_color "CA3626"
    pdf.fill_rectangle [0, 725], 540, 200
    pdf.bounding_box([50, 725], width: 400, height: 150) do
      pdf.fill_color "FFFFFF"
      pdf.move_down 55
      pdf.font_size(30) {pdf.text @lang_json['Pressure_Booster_Report']}
      pdf.move_down 15
      if @company_name
        pdf.font_size(18) {pdf.text "Prepared for #{@company_name}"}
      end
      if @job_title
        pdf.font_size(15) {pdf.text "Job Title/ID: #{@job_title}"}
      end
    end
    pdf.move_down 200
    pdf.image logo, width: 250, position: :center
  end

  def pressure_booster_section(pdf)
    pdf.start_new_page
    # stroke_axis
    pdf.fill_color "40404A"
    pdf.font_size(16) {pdf.text @lang_json['Pressure_Booster']}
    pdf.font_size(10) {
      pdf.move_down 5
      pdf.text @lang_json['Pressure_Booster_Description']
    }


    # Discharge Items
    pdf.move_down 25
    horizontal_line(pdf, 'CA3626')
    pdf.move_down 10
    pdf.font_size(14) {pdf.text @lang_json['Discharge_Items']}

    data = [
        ['','',@lang_json['Input'], @lang_json['Units']],
        ['A', @lang_json['Pressure_Booster_Item_A'], @input_pb['a'], @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['B', @lang_json['Pressure_Booster_Item_B'], @input_pb['b'], @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['C', @lang_json['Pressure_Booster_Item_C'], @input_pb['c'], @lang_json['Feet']],
        ['D', @lang_json['Pressure_Booster_Item_D'], @input_pb['d'], @lang_json['Feet']],
        ['E', @lang_json['Pressure_Booster_Item_E'], @input_pb['e'], @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['F', @lang_json['Pressure_Booster_Item_F'], @input_pb['f'], @lang_json['Pound_Per_Square_Inch_Gauge']]
    ]
    pdf.table(data, column_widths: [25, 385, 65, 65], cell_style: { valign: :center, borders: [:bottom], border_color: 'EEEEEE' }, )


    # Suction Items
    pdf.move_down 25
    horizontal_line(pdf, 'CA3626')
    pdf.move_down 10
    pdf.font_size(14) {pdf.text @lang_json['Suction_Items']}

    data = [
        ['','',@lang_json['Input'], @lang_json['Units']],
        ['G', @lang_json['Pressure_Booster_Item_G'], @input_pb['g'], @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['H', @lang_json['Pressure_Booster_Item_H'], @input_pb['h'], @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['I', @lang_json['Pressure_Booster_Item_I'], @input_pb['i'], @lang_json['Feet']],
        ['J', @lang_json['Pressure_Booster_Item_J'], @input_pb['j'], @lang_json['Feet']],
        ['K', @lang_json['Pressure_Booster_Item_K'], @input_pb['k'], @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['L', @lang_json['Pressure_Booster_Item_L'], @input_pb['l'], @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['M', @lang_json['Pressure_Booster_Item_M'], @input_pb['m'], @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['N', @lang_json['Pressure_Booster_Item_N'], @input_pb['n'], @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['O', @lang_json['Pressure_Booster_Item_O'], @input_pb['o'], @lang_json['Pound_Per_Square_Inch_Gauge']]
    ]
    pdf.table(data, column_widths: [25, 385, 65, 65], cell_style: { valign: :center, borders: [:bottom], border_color: 'EEEEEE' }, )


    # Design Conditions for Schedule and Selection
    pdf.move_down 25
    horizontal_line(pdf, 'CA3626')
    pdf.move_down 10
    pdf.font_size(14) {pdf.text @lang_json['Design_Conditions_For_Schedule_And_Selection']}

    data = [
        ['','',@lang_json['Solution'], @lang_json['Units']],
        ['P', @lang_json['Pressure_Booster_Item_P'], @solution_pb['p'].round(2), @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['Q', @lang_json['Pressure_Booster_Item_Q'], @solution_pb['q'].round(2), @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['R', @lang_json['Pressure_Booster_Item_R'], @solution_pb['r'].round(2), @lang_json['Pound_Per_Square_Inch_Gauge']]
    ]
    pdf.table(data, column_widths: [25, 385, 65, 65], cell_style: { valign: :center, borders: [:bottom], border_color: 'EEEEEE' }, )


    # Minimum Conditions for energy saving calcs. DO NOT SCHEDULE
    pdf.move_down 25
    horizontal_line(pdf, 'CA3626')
    pdf.move_down 10
    pdf.font_size(14) {pdf.text @lang_json['Minimum_Conditions_For_Energy_Saving_Calcs']}

    data = [
        ['','',@lang_json['Solution'], @lang_json['Units']],
        ['S', @lang_json['Pressure_Booster_Item_S'], @solution_pb['s'].round(2), @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['T', @lang_json['Pressure_Booster_Item_T'], @solution_pb['t'].round(2), @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['U', @lang_json['Pressure_Booster_Item_U'], @solution_pb['u'].round(2), @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['V', @lang_json['Pressure_Booster_Item_V1'], @solution_pb['v1'].round(2), @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['V', @lang_json['Pressure_Booster_Item_V2'], @solution_pb['v2'].round(2), @lang_json['Pound_Per_Square_Inch_Gauge']]
    ]
    pdf.table(data, column_widths: [25, 385, 65, 65], cell_style: { valign: :center, borders: [:bottom], border_color: 'EEEEEE' }, )
  end

  def tank_selection_section(pdf)
    pdf.start_new_page
    # stroke_axis
    pdf.fill_color "40404A"
    pdf.font_size(16) {pdf.text @lang_json['Tank_Selection']}
    pdf.font_size(10) {
      pdf.move_down 5
      pdf.text @lang_json['Tank_Selection_Description']
    }


    # How long do you want the booster system off?
    pdf.move_down 25
    horizontal_line(pdf, 'CA3626')
    pdf.move_down 10
    pdf.font_size(14) {pdf.text @lang_json['How_Long_Want_Booster_Off']}

    data = [
        ['','',@lang_json['Input'], @lang_json['Units']],
        ['A', @lang_json['Tank_Selection_Item_A'], @input_ts['a'], @lang_json['Minutes']]
    ]
    pdf.table(data, column_widths: [25, 385, 65, 65], cell_style: { valign: :center, borders: [:bottom], border_color: 'EEEEEE' }, )


    # Type of Building, design flow rate, and location.
    pdf.move_down 25
    horizontal_line(pdf, 'CA3626')
    pdf.move_down 10
    pdf.font_size(14) {pdf.text @lang_json['Type_Of_Building_Design_Flow_Rate_Location']}

    data = [
        ['','',@lang_json['Input'], @lang_json['Units']],
        ['B', @lang_json['Tank_Selection_Item_B'], @input_ts['b'], @lang_json['Gallons_Per_Minute']],
        ['C', @lang_json['Tank_Selection_Item_C'], @building_type, ''],
        ['D', @lang_json['Tank_Selection_Item_D'], @pump_location, '']
    ]
    pdf.table(data, column_widths: [25, 385, 65, 65], cell_style: { valign: :center, borders: [:bottom], border_color: 'EEEEEE' }, )


    # Design conditions and tank size for scheduling
    pdf.move_down 25
    horizontal_line(pdf, 'CA3626')
    pdf.move_down 10
    pdf.font_size(14) {pdf.text @lang_json['Design_Conditions_Tank_Size_For_Scheduling']}

    data = [
        ['','',@lang_json['Solutions'], @lang_json['Units']],
        ['E', @lang_json['Tank_Selection_Item_E'], @solution_ts['e'].round(2), @lang_json['Gallons']],
        ['F', @lang_json['Tank_Selection_Item_F'], @solution_ts['f'].round(2), @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['G', @lang_json['Tank_Selection_Item_G'], @solution_ts['g'].round(2), @lang_json['Pound_Per_Square_Inch_Gauge']],
        ['H', @lang_json['Tank_Selection_Item_H'], @solution_ts['h'].round(2), ''],
        ['I', @lang_json['Tank_Selection_Item_I'], @solution_ts['i'].round(2), @lang_json['Gallons']]
    ]
    pdf.table(data, column_widths: [25, 385, 65, 65], cell_style: { valign: :center, borders: [:bottom], border_color: 'EEEEEE' }, )


    # Select your tank
    pdf.move_down 25
    horizontal_line(pdf, 'CA3626')
    pdf.move_down 10
    pdf.font_size(14) {pdf.text @lang_json['Select_Your_Tank']}

    data = [
        ['','',@lang_json['Tank_Vol'], @lang_json['BG_Model']],
        ['J', @lang_json['Tank_Selection_Item_J'], @solution_ts['j'].round(2), @solution_ts['j_desc']]
    ]
    pdf.table(data, column_widths: [25, 335, 75, 105], cell_style: { valign: :center, borders: [:bottom], border_color: 'EEEEEE' }, )

  end

  def horizontal_line(pdf, color)
    pdf.stroke_color color
    pdf.stroke do
      pdf.horizontal_rule
    end
  end
end