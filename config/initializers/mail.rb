ActionMailer::Base.smtp_settings = {
    :user_name => ENV['MANDRILL_API_USERNAME'],
    :password => ENV['MANDRILL_API_KEY'],
    :address => "smtp.mandrillapp.com",
    :enable_starttls_auto => true,
    :authentication => 'login',
    :port => 2525
}