class CreateTanks < ActiveRecord::Migration[4.2]
  def change
    create_table :tanks do |t|
      t.integer :volume
      t.string :description
      t.decimal :max_acceptance_factor, :precision => 3, :scale => 2
      t.integer :diameter
      t.integer :height

      t.timestamps null: false
    end
  end
end
