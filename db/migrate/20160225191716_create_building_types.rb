class CreateBuildingTypes < ActiveRecord::Migration[4.2]
  def change
    create_table :building_types do |t|
      t.string :name
      t.integer :multiplier

      t.timestamps null: false
    end
  end
end
