class AddLinkToTanks < ActiveRecord::Migration[4.2]
  def change
    add_column :tanks, :link, :string
  end
end
