class CreateSentReports < ActiveRecord::Migration[4.2]
  def change
    create_table :sent_reports do |t|
      t.string :sent_to
      t.string :company_name
      t.attachment :document

      t.timestamps null: false
    end
  end
end
