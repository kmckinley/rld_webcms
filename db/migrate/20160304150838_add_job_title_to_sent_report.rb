class AddJobTitleToSentReport < ActiveRecord::Migration[4.2]
  def change
    add_column :sent_reports, :job_title, :string
  end
end
