class AddZipCodeToSentReport < ActiveRecord::Migration[4.2]
  def change
    add_column :sent_reports, :postal_code, :string
  end
end
