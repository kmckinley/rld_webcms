FactoryGirl.define do
  factory :tank do
    volume 1
    description "MyString"
    max_acceptance_factor "9.99"
    diameter 1
    height 1
  end
end
