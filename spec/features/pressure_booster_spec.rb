require 'rails_helper'

feature 'Tabs', js: true do

  before do

  end

  scenario 'Pressure Booster Tab' do
    visit '/'
    click_on 'ts_tab'
    sleep 1
    click_on 'help_tab'
    sleep 1
    click_on 'pb_tab'

    expect(page).to have_content('Pressure Booster Calculation Sheet')
    expect(page).to have_content('Discharge Items')
    expect(page).to have_content('Suction Items')
    expect(page).to have_content('Design Conditions for Schedule and Selection')
    expect(page).to have_content('Minimum Conditions for energy saving calcs. DO NOT SCHEDULE')
  end

  scenario 'Tank Selection Tab' do
    visit '/'
    click_on 'ts_tab'
    sleep 1
    click_on 'help_tab'
    sleep 1
    click_on 'pb_tab'
    sleep 1
    click_on 'ts_tab'

    expect(page).to have_content('Tank Selection')
    expect(page).to have_content('How long do you want the booster system off?')
    expect(page).to have_content('Type of Building, design flow rate, and location.')
    expect(page).to have_content('Design conditions and tank size for scheduling')
    expect(page).to have_content('Select your tank')
  end

  scenario 'Help Tab' do
    visit '/'
    click_on 'ts_tab'
    sleep 1
    click_on 'help_tab'
    sleep 1
    click_on 'pb_tab'
    sleep 1
    click_on 'help_tab'

    expect(page).to have_content('Item A through F, you enter the discharge values of your system. The following hints may help you.')
    expect(page).to have_content('Item G through O, you enter the suction values of your system. The following hints may help you.')
    expect(page).to have_content('Item P through R are the design conditions you can forward to Deppmann for the pump selection. This is what you schedule for pressures.')
  end
end