describe "HomeController", ->
  scope        = null
  ctrl         = null
  location     = null
  routeParams  = null
  resource     = null


  setupController =(keywords)->
    inject(($location, $routeParams, $rootScope, $resource, $controller)->
      scope       = $rootScope.$new()
      location    = $location
      resource    = $resource
      routeParams = $routeParams
      routeParams.keywords = keywords

      ctrl        = $controller('HomeController',
                                $scope: scope
                                $location: location)
    )

  beforeEach(module("rld"))
  beforeEach(setupController())

  it 'defaults pressure booster inputs', ->
    expect(scope.input_pb).toEqualData({
      a: 40,b: 45,c: 108,d: 200,e: 5,f: 0,g: 40,h: 65,i: 0,j: 20,k: 5,l: 0,m: 12,n: 9,o: 0
    })

  it 'default pressure booster solutions', ->
    expect(scope.solution_pb).toEqualData({
      p: 107.15324675324675, q: 22, r: 85.15324675324675, s: 86.75324675324674, t: 56, u: 30.753246753246742, v1: 86.75324675324674, v2: 39.99999999999999
    })

  it 'perform pressure booster calculations 1', ->
    scope.input_pb.a = 25
    scope.input_pb.b = 40
    scope.input_pb.c = 75
    scope.input_pb.d = 150
    scope.input_pb.e = 3
    scope.input_pb.f = 5
    scope.input_pb.g = 35
    scope.input_pb.h = 55
    scope.input_pb.i = 5
    scope.input_pb.j = 15
    scope.input_pb.k = 8
    scope.input_pb.l = 2
    scope.input_pb.m = 8
    scope.input_pb.n = 13
    scope.input_pb.o = 10
    scope.input_pb_changed()
    expect(scope.solution_pb).toEqualData({
      p: 84.21753246753246, q: 6.235497835497831, r: 77.98203463203464, s: 57.467532467532465, t: 37.83549783549783, u: 19.63203463203463, v1: 57.467532467532465, v2: 25
    })


  it 'defaults tank selection inputs', ->
    expect(scope.input_ts).toEqualData({
      a: 15,b: 200
    })
    expect(scope.tank_locations).toEqualData(
      {
        list: [
          {id: 1, name: 'Discharge'},
          {id: 2, name: 'Top'}
        ],
        selected: {id: 1, name: 'Discharge'}
      }
    )

  it 'default tank selection solutions', ->
    expect(scope.solution_ts).toEqualData(
      {
        e: NaN, f: 86.75324675324674, g: 107.15324675324675, h: 0.1674144968932184, i: NaN, j: 0, j_desc: 'none', j_link: null
      }
    )
